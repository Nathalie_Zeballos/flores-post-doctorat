---
title: "RENECOFOR data"
author: "N Zeballos"
date: "2024-11-25"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(include = FALSE)
```

```{r include=FALSE}
library(tidyverse)
library(lubridate)
library(janitor)
```

#I_ Prepare data
## 1- Open files 
Pathways
```{r include=FALSE}
WD <- getwd()
D <- c("~/Documents/03_Post_doc_Foret/04_Data/01_Raw_data/01_Species/Quercus")
FROM_VENNER <- file.path("Collaborators_datasets", "From_S_Venner")
# FCT <- file.path(WD, "FCT")
# INPUT <- file.path(WD, "INPUT")
# OUTPUT <- file.path(WD, "OUTPUT")
# PLOT <- file.path(WD, "PLOT")
# CHECK <- file.path(PLOT, "CHECK")
# put on another R script
# A <- c("~/Documents/03_Post_doc_Foret/04_Data/01_Raw_data/01_Species/Abies") 
#try_raw_abies <- read_delim(file.path(A,"Online_data_base", "TRY", "32431_04042024180834", "32431.txt"))
```
Renecofor sites:
```{r}
renecofor_sites <- read_delim(file.path(D,FROM_VENNER, "RENECOFOR","RenecoforData_2022", "Situation_placette.txt" ), 
                              delim=";", 
                              locale = locale(encoding = "ISO-8859-1")) %>% 
  clean_names() %>% 
  mutate(code_placette = as_factor(code_placette))
```
-> useful infos:  
- How old is the forest ? annee_naissance_moyenne_an

Inventory:
```{r}
inventory_pop <- read_delim(file.path(D,FROM_VENNER, "RENECOFOR","RenecoforData_2022", "Dendro_peuplement.txt" ), 
                              delim=";", 
                              locale = locale(encoding = "ISO-8859-1")) %>% 
  clean_names()  %>% 
  rename( surface_terriere_m2_ha = surface_terriere_c130_15_cm_m2_ha) %>% 
  mutate(code_placette = as_factor(code_placette))

inventory_trees <- read_tsv(file.path(D,FROM_VENNER, "RENECOFOR","RenecoforData_2022", "Dendro_inv_arbre.txt" ), locale = locale(encoding = "ISO-8859-1") )  %>% 
  clean_names()  %>% 
  mutate(code_placette = as_factor(code_placette))
```

Litter 
```{r}
new_names <- c("code_placette", "lit_datdeb", "lit_datfin", "lit_nbcol" , "lit_nbfruct_ha" , "lit_nbfruct_av_ha" , "lit_feuil_ha", "lit_bran_ha" , "lit_fruct_ha", "lit_fruct_av_ha",  "lit_reste_ha", "lit_ess_sec_1", "lit_feuil_es1_ha", "lit_ess_sec_2", "lit_feuil_es2_ha")

renecofor_litter <- read_tsv(file.path(D,FROM_VENNER, "RENECOFOR","RenecoforData_2022", "Litiere_flux_ha_UTF8.txt" )) %>% 
  rename_with(~ new_names, everything()) %>% #variables with shorter and R-friendly names
  mutate(lit_datdeb = dmy(lit_datdeb), 
         lit_datfin = dmy(lit_datfin),
         an = year(lit_datdeb),               #extract year of sampling
         mois = month(lit_datdeb))             #extract month of sampling
  
#### Later, for Q. robur and A. alba

# renecofor_litter_qhybride_rp <- renecofor_litter %>% filter(str_starts(code_placette, "CPS")) #chêne pédonculé sessile  (hybride)
# renecofor_litter_aalba <- renecofor_litter %>% filter(str_starts(code_placette, "SP"))
# 
# summary(renecofor_litter_qrobur) 
# renecofor_litter_qrobur %>% count(an) 
# renecofor_litter_aalba %>% count(an)

#renecofor_litter <-  read_csv2(file.path(D,FROM_VENNER, "RENECOFOR","data_RENECOFOR_Isabelle_litiere", "litieres_renecofor.csv" ))
# This file is not updated and do not contain the fils from the last years son we don't use it. 
```

## 2- Tidy & explore datasets

### Litter - Q. petraea
Explore litter data set.
```{r}
renecofor_litter %>% filter(str_starts(code_placette, "CHS") & !is.na(lit_ess_sec_1) ) %>% 
  select(code_placette, lit_ess_sec_1) %>% unique()
#to remove observations with value for secondary species :  & !is.na(lit_ess_sec_1) 
renecofor_litter %>% filter(str_starts(code_placette, "CHP") & !is.na(lit_ess_sec_1) ) %>% 
  select(code_placette, lit_ess_sec_1) %>% unique()
renecofor_litter %>% filter(str_starts(code_placette, "CHP")) %>% 
  select(code_placette) %>% unique()
# 9 placettes
renecofor_litter %>% filter(str_starts(code_placette, "CPS") & !is.na(lit_ess_sec_1) ) %>% 
  select(code_placette) %>% unique() 
# 2 placettes sur les 2
```
-> Toutes les placettes ont des observations avec l'essence secondaire principale qui n'ets pas un autre chêne c(est soit charme ou hêtre). Donc ne pas utiliser ce critère pour filtrer? 
Peut-être enlever les placettes qui en moyenne ont la biomasse de l'essence secondaire > X % du total, proportion à définir avec Isabelle, pour quand même prendre en compte cette idée que la biomasse de l'essence principale doit être représentative par rapport à la surface de la placette. 

Filtre sur l'essence principale:
```{r include=FALSE}
renecofor_litter_qpetraea <- renecofor_litter %>% filter(str_starts(code_placette, "CHS") ) #
renecofor_litter_qpetraea %>% count(code_placette, an)
```
#### Filtre Essence secondaire
```{r}
renecofor_litter <- renecofor_litter %>% mutate(secondaire_principale_lit_feuil_ha = lit_feuil_es1_ha/lit_feuil_ha)
renecofor_litter_qpetraea <- renecofor_litter %>% filter(str_starts(code_placette, "CHS") & secondaire_principale_lit_feuil_ha < 0.2 ) 
nrow(renecofor_litter_qpetraea) # nobs = 159!!!
```


Observations for the year where only 1 or 2 samplings per sites has been done: 
-> Remove these observations from the data set since we use the cumulative leaf biomass over the year, 1 or 2 samplings per year are not representative. (also for cumulative stem biomass )
```{r echo=FALSE}
combinations_to_keep <- renecofor_litter_qpetraea %>% count(code_placette, an) %>%
  filter(n >2) %>% select(code_placette, an) # Combinations year-site to keep

renecofor_litter_qpetraea <- renecofor_litter_qpetraea %>% semi_join(combinations_to_keep, by = c("code_placette", "an"))
#renecofor_litter_qpetraea %>% count(code_placette, an) %>%
#  filter(n <3) %>% select(code_placette, an) # Check remove observatins with only 1 or 2 sampling per site and per year.
#n 1045
```

```{r include=FALSE}
renecofor_litter_qpetraea %>% filter(if_any(lit_feuil_ha, is.na)) # NA for variable of interest : remove.
renecofor_litter_qpetraea <- renecofor_litter_qpetraea %>% 
  filter(!( code_placette == "CHS 03" & lit_datdeb == "2005-05-25"| code_placette == "CHS 81" & lit_datdeb == "2000-12-14")) #remove NA.
#renecofor_litter_qpetraea <- renecofor_litter_qpetraea %>% filter(lit_feuil_ha!= 0)
# 8 observations avec biomasse feuille = 0: on les garde car on utilise le cumul dans l'année.
```
Statistics on the data set after cleaning.
```{r}
print (paste("Nb of observations:", nrow(renecofor_litter_qpetraea))) #1043
renecofor_litter_qpetraea %>% summarise(min_an = min(an), max_an = max(an))
```
#### Leaf biomass
```{r echo=FALSE}
# renecofor_litter_qpetraea %>% group_by(mois) %>% 
#   summarise(mean_leaf_biomass = mean(lit_feuil_ha, na.rm=T),
#             sd_leaf_biomass = sd(lit_feuil_ha, na.rm=T),
#             min_leaf_biomass = min(lit_feuil_ha, na.rm=T),
#             max_leaf_biomass = max(lit_feuil_ha, na.rm=T))

# Checking values distribution.
ggplot(data= renecofor_litter_qpetraea) + geom_histogram(aes(lit_feuil_ha)) + 
  facet_grid(~mois)
```
### Renecofor sites
Q. petraea


#II_ Inputs .species file
Compute inputs for .species file.

### 1- a_unpollinatedFemale
#### 1. Cumulative leaf biomass.ha over the year
```{r include=FALSE}
# Cumulative leaf biomass.ha over the year per site.
leaf_litter_year_qpetraea <- renecofor_litter_qpetraea %>% group_by(code_placette, an) %>% 
  summarise(leaf_biomass_year_ha = sum(lit_feuil_ha) )
leaf_litter_year_qpetraea       
ggplot(data= leaf_litter_year_qpetraea) + geom_histogram(aes(leaf_biomass_year_ha))
```
```{r echo=FALSE}
# Mean cumulative leaf biomasse.ha per year over all sites
leaf_litter_year_qpetraea %>% ungroup() %>% 
  summarise(mean_leaf_biomass_year_ha = mean(leaf_biomass_year_ha),
            sd_leaf_biomass_year_ha = sd(leaf_biomass_year_ha),
            min_leaf_biomass_year_ha = min(leaf_biomass_year_ha),
            max_leaf_biomass_year_ha = max(leaf_biomass_year_ha) ) %>% 
  mutate(mean_leaf_biomass_year_m2 = mean_leaf_biomass_year_ha*0.1,
         sd_leaf_biomass_year_m2 = sd_leaf_biomass_year_ha*0.1,    ## ??
         min_leaf_biomass_year_m2 = min_leaf_biomass_year_ha*0.1, 
         max_leaf_biomass_year_m2 =max_leaf_biomass_year_ha*0.1) 
```

From RENECOFOR dataset (sites CHS 1994-2017): cumulative over the year leaf biomass between 350-4800 kg/ha, i.e. 35-478 kg/m2, and mean = 251 kg/m2.
We computed the number of males flowers depending on leaf biomass over this range using a linear relationship with coefficients  a_potMaleFlo (112) and b_potMaleFlo (19148) (values from Q. ilex) and obtained 23100-72700 male flowers per m².
Redo this step if I can get values of a_potMaleFlo et b_potMaleFlo for Q. petraea (by Nicolas Delpierre).

#### 2. Number of potential males flowers 
Fct Male flowers numbers
```{r}
a_potMaleFlo <- 112   #values from Le Roncé 2020 for Q. ilex
b_potMaleFlo <- 19148
nb_Male_flowers <- function(x) {
  y = a_potMaleFlo*x +b_potMaleFlo
  return(y)
}
```

```{r}
nb_Male_flowers_data <- tibble(leaf_biomass_m2 = seq(35, 480), 
                               nb_male_flowers_m2 = nb_Male_flowers(leaf_biomass_m2))
nb_Male_flowers(35) 
nb_Male_flowers(478)
ggplot(nb_Male_flowers_data)+ geom_line(aes(x= leaf_biomass_m2, y=nb_male_flowers_m2))
```
-> 23100-72700 male flowers per m². 
Mean = 47900 ~ 48000 male flowers per m².

#### 3. Unpollinated female flowers
Fct unpollinated Female flowers
```{r}
# /!\ Check with Isabelle the minus sign for a
a_unpollinatedFemale <- 0.00006   #values obtained by testing   
b_unpollinatedFemale <- 48000
unpollinated_Female_flowers <- function(x) {
  y = 1/(1 + exp(a_unpollinatedFemale *(x - b_unpollinatedFemale)))
  return(y)
}
```
Then we modified the sigmoid parameters (a,b) to have 
-that the sigmoid look like ~ linear relation
-50% of unpollinated female flowers is computed for the mean of the range of male flowers ~ 48000, which mean given the value of 48 000 for the abscissa of the inflexion point (b_unpollinatedFemale ).
```{r}
unpollinated_Female_flowers_data <- tibble(nb_male_flowers_m2 = seq(10000, 80000), 
                               unpollinated_female_flowers = unpollinated_Female_flowers(nb_male_flowers_m2))
ggplot(unpollinated_Female_flowers_data)+ geom_line(aes(x= nb_male_flowers_m2, y=unpollinated_female_flowers))
```

#### Choosen value a_unpollinatedFemale
a_unpollinatedFemale <- 0.00006
b_unpollinatedFemale <- 48000

#### Q. robur
```{r}
renecofor_litter_qrobur <- renecofor_litter %>% filter(str_starts(code_placette, "CHP"))
combinations_to_keep <- renecofor_litter_qrobur %>% count(code_placette, an) %>%
  filter(n >2) %>% select(code_placette, an) # Combinations year-site to keep

renecofor_litter_qrobur <- renecofor_litter_qrobur %>% semi_join(combinations_to_keep, by = c("code_placette", "an"))
renecofor_litter_qrobur <- renecofor_litter_qrobur %>% filter(!if_any(lit_feuil_ha, is.na)) # NA for variable of interest : remove too.
```
 1- Cumulative leaf biomass.ha over the year
```{r include=FALSE}
# Cumulative leaf biomass.ha over the year per site.
leaf_litter_year_qrobur  <- renecofor_litter_qrobur %>% group_by(code, an) %>% 
  summarise(leaf_biomass_year_ha = sum(lit_feuil_ha, na.rm= T) )
leaf_litter_year_qrobur       
ggplot(data= leaf_litter_year_qrobur) + geom_histogram(aes(leaf_biomass_year_ha))
```
-> Distribution of cumulative leaf biomass for Q. robur is similar to Q. petraea so we will use the computation and results obtained on the sites of Q. petraea for robur and pubescens regarding the inputs "a_unpollinatedFemale" and "b_unpollinatedFemale".


### 2- a_potFemaleFlo
Flower data set:
```{r}
load(file.path(D,FROM_VENNER, "tauxfructif_popan.Rda" )) # (named "flfrmoypopan")
renecofor_flower <- flfrmoypopan %>% 
  rename( code_placette = Site, an = Year) #%>% #variables with shorter and R-friendly names
unique(renecofor_flower$code_placette)
sites_flower <- renecofor_flower %>% select(code_placette) %>% distinct() %>% pull() 
sites_flower
```
Data set send by Marie-Claude Venner, observations from 2013 to 2023 for 11 sites of sessile oak.  
Le bilan par site.année des variables suivantes:  
- flomoypop: le nombre moyen de fleurs totales (tous stades jusqu'au fruit mature) par m² (moyen pour les 10 arbres du site, en général, parfois c'est sur moins d'arbres).  
- fruitmoypop: le nombre moyen de fruits matures par m² (moyen pour les 10 arbres du site, en général, parfois c'est sur moins d'arbres).  
- fructmoypop: proportion moyenne de fleurs qui ont donné un fruit mature.  
- fecDsurAmoypop: proportion moyenne de fleurs qui ont atteint au moins le stade grosse fleur (D).

Leaf data set:
```{r}
#Delete the white space in code column to join with the other data set 'renecofor_flower'
leaf_litter_year_qpetraea <- leaf_litter_year_qpetraea %>%
  mutate(code_placette = factor(str_remove_all(as.character(code_placette), " "))) %>% 
  mutate(leaf_biomass_year_m2 = leaf_biomass_year_ha*0.1)
```
Obs 1995-2017, mais obs de 2008-2017 uniquement sur le site CHS41.

```{r}
inner_join(renecofor_flower, leaf_litter_year_qpetraea, by = c("code", "an"))
```
-> Pas de correspondance en années entre les données de biomasse de feuilles et les biomasses de fleurs femelles: les échantillonnages de ces dernières ont eu lieu entre 2013-2023 alors que les litières de feuilles se sont arretés en 2013 (sauf pour 1 parcelle qui n'a pas été échantllionnée pour les fleurs).

On cherche alors des correspondance de parcelles à 2 ans près, mais le gap minimum est de 5 ans au final cad des biomasse de feuilles pour l'année 2007 à mettre en relation avec biomasses fleurs femelles de 2013:
```{r}
leaf_litter_year_qpetraea %>% filter(an == 2007) %>% select(code_placette) %>% pull()
```
2008-2012 : only site CHS41!
2007 : 16 sites pour biomasse feuilles.
```{r}
renecofor_flower %>% filter(an == 2013) %>% select(code_placette) %>% pull()
```
2013: 11 sites pour biomasse fleurs.

/!\ Trouver laquelle de parcelle (CHS57a CHS57b) correspond à CHS57 dans le data set de fleurs.
Joindre les 2 jeux de données:
```{r}
renecofor_flower_leaf<- inner_join(renecofor_flower[renecofor_flower$an== 2013,], 
           leaf_litter_year_qpetraea[leaf_litter_year_qpetraea$an == 2007,], 
           by = c("code_placette"))

renecofor_flower_leaf
```
9 parcelles (car las correpsondance pour CHS57 non faite).

```{r}
ggplot(renecofor_flower_leaf)+ geom_point(aes(y=flomoypop, x=leaf_biomass_year_m2)) + 
   geom_smooth(aes(y=flomoypop, x=leaf_biomass_year_m2), 
               method = "lm", se = T) #, color = "blue" FALSE

```

On observe une relation négative! (au lieu de positive comme dans la thèse de Le Roncé 2020).
```{r}
lm_flower_leaf <- lm(flomoypop ~ leaf_biomass_year_m2, data = renecofor_flower_leaf)
lm_flower_leaf
summary(lm_flower_leaf)
```
En essayant avec les années 2013,2014 et 2015 des biomasses des fleurs, on observe soit une relation négative faible soit pas de relation donc on va garder les données de Q. ilex faites par Iris Le Roncé 2020.

### 3- WoodTurnOver
WoodTurnOver = Proportion de bois mort par rapport au bois sur pied par an, donc il faut des données de litière: Lit branches/biomasse bois totale (cad = en fonction de la densité*volume qui est obtenue par dbh et hauteur...). 
Filter data sets criterion:  
- Regarding wood biomass, we remove the sites where the secondary species is not from the same genus, i.e for CHS we keep only if secondary species is another oak.  
- We keep only sites with >=3 samplings per year so the cumulative stem litter is representative of the year (same filter as below for cumulative leaf biomass).  

#### 1. Stem biomass
```{r}
#renecofor_litter_qpetraea

# Cumulative stem biomass.ha over the year per site ad per m2.
stem_litter_year_qpetraea <- renecofor_litter_qpetraea %>% group_by(code_placette, an) %>% 
  summarise(stem_biomass_year_ha = sum(lit_bran_ha) ) %>% 
  mutate(stem_biomass_year_m2 = stem_biomass_year_ha*0.1)
stem_litter_year_qpetraea

ggplot(data= stem_litter_year_qpetraea) + geom_histogram(aes(stem_biomass_year_ha))
```
Normal distribution of the cumulative stem biomass over the year per hectare. 
2 outliers pour valeur maximale: 3000 et 4900 kg/ha alors que le reste est < 2400 kg.ha. Donc enlever ces 2 observations?  

```{r}
stem_litter_year_qpetraea %>% filter(stem_biomass_year_ha>2500)
```

```{r}
renecofor_litter_qpetraea %>% filter(code_placette == "CHS 86" & an == 1997| code_placette == "CHS 86" & an == 2004)
```
/!\ CHS 86: essence secondaire 1ere = hêtre donc enlever aussi pour l'estimation de woodturnover car je ne voudrai pas prendre en compte le volume sur pied de l'essence principale si essence secondaire autre que chêne.

```{r echo=FALSE}
# Mean cumulative stem biomasse.ha per year over all sites
stem_litter_year_qpetraea %>% ungroup() %>% 
  summarise(mean_stem_biomass_year_ha = mean(stem_biomass_year_ha),
            sd_stem_biomass_year_ha = sd(stem_biomass_year_ha),
            min_stem_biomass_year_ha = min(stem_biomass_year_ha),
            max_stem_biomass_year_ha = max(stem_biomass_year_ha),
            ## biomass.m2  
            mean_stem_biomass_year_m2 = mean(stem_biomass_year_m2),
          sd_stem_biomass_year_m2 = sd(stem_biomass_year_m2),    
          min_stem_biomass_year_m2 = min(stem_biomass_year_m2), 
          max_stem_biomass_year_m2 = max(stem_biomass_year_m2)) 
```
From RENECOFOR dataset (sites CHS 1994-2017): cumulative over the year stem biomass between 60-4992 kg/ha, i.e. 6-499 kg/m2, and mean = 954 kg/ha.

#### 2. Wood biomass 
Vallet et al 2006 (infos from liste_paramètres_growth_Phenofit5.xlsx to check):  
Equation to estimate aboveground total volume :  
volume = form / (4000 * 3.14) * cbh^2 * Height  
avec form =(a + b*cbh + c* (racine(cbh)/height)*(1+d/cbh^2)

```{r}
renecofor_inventory <- read_tsv(file.path(D,FROM_VENNER, "RENECOFOR","RenecoforData_2022", "Dendro_inv_arbre.txt" )) 
```

#### Choosen value WoodTurnOver


##III_ Init file
--> Peut-être à mettre dans un script à part. 

Compute values for init file
Leaf biomass
Wood biomass
#### Europe
Initialistaion : valeurs pour avoir une representation d'une chenaie moyenne européenne.

Leaf biomass:
We only used the oldest datas of Renecofor sites, i.e. observations from 1994-2000 which corresponds to trees ~ 70 years old (check if True).

```{r}
leaf_litter_year_qpetraea %>% filter(an <=2000) %>% ungroup() %>% 
  summarise(mean_leaf_biomass_year_ha = mean(leaf_biomass_year_ha),
            sd_leaf_biomass_year_ha = sd(leaf_biomass_year_ha),
            min_leaf_biomass_year_ha = min(leaf_biomass_year_ha),
            max_leaf_biomass_year_ha = max(leaf_biomass_year_ha) ) %>% 
  mutate(mean_leaf_biomass_year_g_m2 = mean_leaf_biomass_year_ha*100,
         sd_leaf_biomass_year_g_m2 = sd_leaf_biomass_year_ha*100,    
         min_leaf_biomass_year_g_m2 = min_leaf_biomass_year_ha*100, 
         max_leaf_biomass_year_g_m2 =max_leaf_biomass_year_ha*100) 
```
Leaf biomass =  247 904 g/ha per year. 

#### Renecofor sites

##### Unused 
Datasets collected non corresponding to our needs, i.e. biomass of female flowers only. 
```{r}
#Nb d'observations for lit_nbfruct_ha & lit_nbfruct_av_ha =!=Na
renecofor_litter_qpetraea %>% summarise(across(c(lit_nbfruct_ha, lit_nbfruct_av_ha)))
renecofor_litter_qpetraea <- renecofor_litter_qpetraea %>%  mutate(lit_nbfruct_and_av_ha = lit_nbfruct_ha+lit_nbfruct_av_ha )
```


```{r}
fig_potFemaleFlo_qpetraea <- ggplot(renecofor_litter_qpetraea) + geom_point(aes(x= lit_feuil_ha, y = lit_nbfruct_ha+lit_nbfruct_av_ha, colour= as.factor(an)) )
fig_potFemaleFlo_qpetraea
```
```{r}
fig_potFemaleFlo_qpetraea <- ggplot(renecofor_litter_qpetraea) + geom_point(aes(x= lit_feuil_ha, y = (lit_nbfruct_ha), colour= as.factor(an)) )
fig_potFemaleFlo_qpetraea
```


```{r}
lm_potFemaleFlo_qpetraea <- lm(data = renecofor_litter_qpetraea, lit_nbfruct_ha ~ lit_feuil_ha)
lm_potFemaleFlo_qpetraea
summary(lm_potFemaleFlo_qpetraea)
```
/!\ Avant 2006, la variable "lit mas fruct" réfère à la pesée des fruits viables ET non-viables.
A partir de 2006, séparation des fruits viables (="lit_nbfruct_ha") et des fruits non viables (="lit_nbfruct_av_ha").

```{r}
lm_potFemaleFlo_qpetraea <- lm(data = renecofor_litter_qpetraea, lit_nbfruct_ha ~ lit_feuil_ha)
lm_potFemaleFlo_qpetraea
summary(lm_potFemaleFlo_qpetraea)
```

