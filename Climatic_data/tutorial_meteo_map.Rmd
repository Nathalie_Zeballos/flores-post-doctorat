---
title: "Tutorial meteorogical spatial data"
author: "Nathalie Zeballos"
date: '2024-04-04'
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
#library(geodata)
library(terra)
```
```{r}
#Meteo France Open access tutorial
library(sf)
library(tidyverse)
library(httr)
library(glue)
library(janitor)
library(jsonlite)
```



## Meteo France Open access tutorial
[Tutorial](https://www.r-bloggers.com/2023/12/meteo-france-open-data/?utm_source=phpLis)
```{r}
round_any <- function(x, accuracy, f = round) {
  f(x / accuracy) * accuracy
}
```

Select a few départements, here the 12 from Auvergne-Rhône-Alpes.
```{r}
# which départements to download
sel_dep <- c("01", "03", "07", "15", "26", "38", "42", "43", "63", "69", "73", "74")
# for the map (code INSEE région)
sel_reg <- "84"
```

### Get info
Get some information about the available files for the daily data [(Données climatologiques de base - quotidiennes).](https://meteo.data.gouv.fr/datasets/6569b51ae64326786e4e8e1a)
```{r}
# count files available
#GET a url
n_files <- GET("https://www.data.gouv.fr/api/2/datasets/6569b51ae64326786e4e8e1a/") %>%
  content() %>%
  pluck("resources", "total")

# get files informations
files_available <- GET(glue("https://www.data.gouv.fr/api/2/datasets/6569b51ae64326786e4e8e1a/resources/?page=1&page_size={n_files}&type=main")) %>% 
  content(as = "text", encoding = "UTF-8") %>% 
  fromJSON(flatten = TRUE) %>% 
  pluck("data") %>%    #extract the table included in the data section of the file (list)
  as_tibble(.name_repair = make_clean_names)

```

Download and open the files
```{r}
#Create a repository "data" where the files will be downloaded
if (!length(list.files("data"))) {
  files_available %>%  
    #add variable: department number
    mutate(dep = str_extract(title, "(?<=departement_)[:alnum:]{2,3}(?=_)")) %>%  
    #select on department & title
    filter(dep %in% sel_dep,
           str_detect(title, "RR-T-Vent")) %>% 
    # Function to map over multiple input simultaneously (in "parallel")
    pwalk(\(url, title, format, ...) {
      #
      GET(url,
          #save a file that's bigger than memory, use write_disk() to save it to a known path.
          write_disk(glue("data/{title}.{format}"),
                     overwrite = TRUE))
  })
}
```

We get 36 files…Each département has 3 files representing the recent years, 1950-2021 and before 1950.
```{r}
# parsing problems with readr::read_delim
# we use read.delim instead
meteo <- list.files("data", full.names = TRUE) |> 
  map(read.delim,
      sep = ";",
      colClasses = c("NUM_POSTE" = "character",
                     "AAAAMMJJ" = "character")) |> 
  list_rbind() |> 
  as_tibble(.name_repair = make_clean_names) |>
  mutate(num_poste = str_trim(num_poste),
         aaaammjj = ymd(aaaammjj))

# Map data
# See https://r.iresmi.net/posts/2021/simplifying_polygons_layers/
dep_sig <- read_sf("adminexpress_simpl_2022/adminexpress_cog_simpl_000_2022.gpkg",
#adminexpress_cog_simpl_000_2022.gpkg
                   layer = "departement") |> 
  filter(insee_reg == sel_reg) |> 
  st_transform("EPSG:2154")
```

# Exploring
Figure 1: Weather stations in Auvergne-Rhône-Alpes
```{r}

meteo |> 
  summarise(.by= c(nom_usuel, num_poste, lat, lon),
            annee_max = max(year(aaaammjj))) |> 
  st_as_sf(coords = c("lon", "lat"), crs = "EPSG:4326") |> 
  st_transform("EPSG:2154") |> 
  ggplot() +
  geom_sf(data = dep_sig) +
  geom_sf(aes(color = annee_max == year(Sys.Date()),
              shape = annee_max == year(Sys.Date())),
          size = 2, alpha = 0.5) +
  scale_color_manual(name = "statut",
                     values = c("FALSE" = "chocolate4",
                                "TRUE" = "chartreuse3"),
                     labels = c("FALSE" = "inactive",
                                "TRUE" = "active")) +
  scale_shape_manual(name = "statut",
                     values = c("FALSE" = 16,
                                "TRUE" = 17),
                     labels = c("FALSE" = "inactive",
                                "TRUE" = "active")) +
  labs(title = "Stations météo",
       subtitle = "Auvergne-Rhône-Alpes",
       caption = glue("données Météo-France (https://meteo.data.gouv.fr/)
                      carto : d'après IGN Admin Express 2022
                      https://r.iresmi.net/ {Sys.Date()}")) +
  guides(color = guide_legend(reverse = TRUE),
         shape = guide_legend(reverse = TRUE)) +
  theme_minimal() +
  theme(plot.caption = element_text(size = 6))
```

What would be an interesting station?
```{r}
# longest temperature time series (possibly discontinuous)
meteo |>
  filter(!is.na(tx)) |>
  summarise(.by = c(nom_usuel, num_poste),
            deb = min(year(aaaammjj)),
            fin = max(year(aaaammjj)),
            n_annee = n_distinct(year(aaaammjj))) |>
  mutate(etendue = fin - deb) |>
  arrange(desc(n_annee))
```

Choosing the longest time series:
Figure 2: oldest/longest regional temperature series
```{r}
meteo |>
  filter(num_poste == "69204002") |>
  mutate(tm = if_else(is.na(tm), (tx + tn) / 2, tm)) |> 
  select(num_poste, nom_usuel, aaaammjj, tn, tm, tx) |> 
  pivot_longer(c(tn, tm, tx),
               names_to = "mesure", 
               values_to = "temp") |> 
  mutate(mesure = factor(mesure, 
                       levels = c("tx", "tm", "tn"),
                       labels = c("Tmax", "Tmoy", "Tmin"))) %>% 
  {
    ggplot(data = ., aes(aaaammjj, temp, color = mesure)) +
    geom_point(size = 1, alpha = 0.01) +
    geom_smooth(method = "gam", 
                formula = y ~ s(x, bs = "cs", k = 30)) +
    scale_x_date(date_breaks = "10 years", 
                 date_labels = "%Y",expand = expansion(),
                 limits = ymd(paste0(c(
                   round_any(min(year(.$aaaammjj), na.rm = TRUE), 10, floor),
                   round_any(max(year(.$aaaammjj), na.rm = TRUE), 10, ceiling)), "0101"))) +
    # scale_y_continuous(breaks = scales::breaks_pretty(4),
    #                    minor_breaks = scales::breaks_pretty(40)) +
    ## Erreur dans pretty.default(x, n, ...) : argument 'n' incorrect  
    scale_color_manual(values = c("Tmin" = "deepskyblue2",
                                  "Tmoy" = "grey50",
                                  "Tmax" = "firebrick2"),
                       labels = list("Tmax" = bquote(T[max]), 
                                     "Tmoy"= bquote(T[moy]),
                                     "Tmin"= bquote(T[min]))) +
    labs(title = glue("{.$nom_usuel[[1]]} ({str_sub(.$num_poste[[1]], 1, 2)})"),
         subtitle = "Températures quotidiennes",
         x = "année",
         y = "℃",
         caption = glue("données Météo-France (https://meteo.data.gouv.fr/)
                        https://r.iresmi.net/ {Sys.Date()}")) +
    theme_minimal() +
    theme(plot.caption = element_text(size = 6),
            axis.title.y = element_text(angle = 0, vjust = 0.5))
  }
```

## Tutorial to create a map on R
https://delladata.fr/tutoriel-cartes-geographique-ggplot2/

```{r cars, eval=FALSE, include=FALSE}
Fr.dep <- gadm(country = "FR" , level=2,  path=tempdir())
plot(Fr.dep) 
```

## Transformer en objet sf
```{r pressure, eval=FALSE, include=FALSE}
Fr.dep <- st_as_sf(Fr.dep)
str(Fr.dep)
```

Plot avec ggplot2 le fond de carte
```{r eval=FALSE, include=FALSE}
g2 <- ggplot(data =Fr.reg) +
  geom_sf(fill="white") +
  theme_classic() +
  theme_void() + #Supprimer les étiquette des coordonnées
  ggtitle("Ma carte") + # ajouter un titre et personnalisation
  theme(plot.title = element_text(hjust = 0.5, 
                                    size=12, 
                                    color="blue", 
                                    face="bold"))
g2 
```

Pour positionner des lieux sur la carte, nous allons devoir fournir leurs coordonnées en longitude (axe des x) et latitude (axe des y) dans le même système de référence.

Le CRS des données provenant du projet geodata est le système WGS 84 ; c’est également celui employé par le gps.

Positionner plusieurs lieux
```{r eval=FALSE, include=FALSE}
#Exemple d'un dataframe: à créer ou ouvrir fichier existant
df <- data.frame(
  ville = c("Paris", "Bordeaux", "Lyon", "Marseille"),
  longitude = c(2.3522, -0.5792, 4.8357, 5.3698),
  latitude = c(48.8566, 44.8378, 45.7640, 43.2965)
) 
```

```{r eval=FALSE, include=FALSE}
ggplot(data = Fr.dep) +
  geom_sf(fill="lightyellow", colour="purple") +
  geom_point(data=df, 
             aes(x=longitude, y=latitude,colour=ville),
             size=3)+
  scale_colour_manual(values=c("orange", "blue", "magenta", "green3"))+
  geom_text(data=df, 
            aes(x=longitude, y=latitude,colour=ville, 
            label = ville), 
            nudge_x = 0.2, nudge_y = 0.4)+
  theme_void()+
  theme(legend.position = "none")+
  ggtitle("Mes sites")+
  theme(plot.title = element_text(hjust = 0.5, 
                                    size=12, 
                                    color="blue", 
                                    face="bold"))
```


```{r eval=FALSE, include=FALSE}
# Meteo stations available
stations_list <- 
  
# Experimental sites for Abies
exp_sites_list 
```

```{r eval=FALSE, include=FALSE}
ggplot(data = Fr.dep) +
  geom_sf(fill="lightyellow", colour="purple") +
  geom_point(data=stations_list, 
             aes(x=longitude, y=latitude),
             size=3)+
  scale_colour_manual(values=c("orange", "blue", "magenta", "green3"))+
  geom_text(data=stations_list, 
            aes(x=longitude, y=latitude, 
            label = ville), 
            nudge_x = 0.2, nudge_y = 0.4)
```
